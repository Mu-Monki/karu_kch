<?php

// TEACHER ROUTES
Route::get('/', 'HomeController@home')->middleware('auth');
Route::get('/home', 'HomeController@home')->middleware('auth');
Route::get('/student_performance', 'HomeController@student_performance')->name('student_performance')->middleware('auth');
Route::get('/make_post', 'HomeController@post_announcement')->name('make_post')->middleware('auth');
Route::post('/store_post', 'AnnouncementController@store')->middleware('auth');
Route::get('/videos', 'HomeController@videos_basabilang')->name('videos')->middleware('auth');
Route::get('/videos_all/{type}', 'HomeController@videos_list_all')->name('videos_all')->middleware('auth');
Route::get('/videos_archive', 'HomeController@videos_archive')->name('videos_archive')->middleware('auth');
Route::get('/videos_set', 'HomeController@videos_set')->name('videos_set')->middleware('auth');
Route::get('/send_feedback', 'HomeController@send_feedback')->middleware('auth');
Route::get('/play_video/{id}', 'HomeController@play_video')->name('play_video')->middleware('auth');
Route::post('/post_feedback', 'FeedbackController@store')->middleware('auth');


//ADMIN ROUTES
Route::post('/bb_partner', 'FeedbackController@bb_partner')->name('bb_partner')->middleware('auth');
Route::post('/save_video', 'VideoController@save_video')->name('save_video')->middleware('auth');
Route::get('/view_feedback', 'HomeController@view_feedback')->middleware('auth');
Route::get('/upload_videos', 'HomeController@upload_videos')->middleware('auth');


//ERROR ROUTES
Route::get('/error', 'HomeController@error')->name('error')->middleware('auth');
Route::get('/401', 'HomeController@error_401')->name('error_401')->middleware('auth');
Route::get('/403', 'HomeController@error_403')->name('error_403')->middleware('auth');
Route::get('/404', 'HomeController@error_404')->name('error_404')->middleware('auth');
Route::get('/500', 'HomeController@error_500')->name('error_500')->middleware('auth');
// Route::get('/construction', 'HomeController@');


//UNIVERSAL
Route::get('/login_karu', 'HomeController@login_karu')->name('login_karu')->middleware('auth');
Route::get('/register_karu', 'HomeController@register_karu')->name('register_karu')->middleware('auth');
Route::get('/schools', 'HomeController@school_partners')->name('school_partners')->middleware('auth');

// AUTHENTICATION ROUTES
Auth::routes();