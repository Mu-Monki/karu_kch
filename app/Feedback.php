<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $primaryKey='feedback_id';
    protected $table='feedback';
    protected $fillable = [
        'title', 'body', 'feedback_type', 'user_id',
    ];
}
