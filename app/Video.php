<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $primaryKey='video_id';
    protected $table='videos';
    protected $fillable = [
        'title', 'video_path', 'thumbnail_path', 'target_grade', 'video_type',
    ];
}
