<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Feedback;

class FeedbackController extends Controller
{
    public function store(Request $request) {
        $feedback = new Feedback();
        
        $feedback->title = $request->title;
        $feedback->body = $request->post_body;
        $feedback->feedback_type = $request->emotion;
        $feedback->user_id = Auth::user()->id;

        $feedback->save();
        //dd($request);
        return redirect()->action('HomeController@home');
    }
}
