<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     return view('pages_teacher.home');
    // }

    public function home() {
        $announcements = DB::table('announcement')->join('users', 'users.id', '=', 'announcement.user_id')->paginate(10);
        //dd($announcements);
        return view('pages_teacher.home')->with('announcements', $announcements);
    }

    public function student_performance() {
        return view('pages_teacher.performance_student');
    }

    public function videos_basabilang() {
        $basa_videos = DB::table('videos')->where('video_type', '=', 'basa')->get();
        $bilang_videos = DB::table('videos')->where('video_type', '=', 'bilang')->get();
        
        //dd([$basa_videos, $bilang_videos]);

        return view('pages_teacher.videos_basa_bilang')
                ->with('basa_videos', $basa_videos)
                ->with('bilang_videos', $bilang_videos);
    }

    public function post_announcement() {
        return view('pages_admin.make_announcement');
    }
    
    public function videos_archive() {
        return view('pages_teacher.videos_archive');
    }

    public function videos_list_all($type) {
        // dd($type);
        $basa_videos = DB::table('videos')->where('video_type', '=', 'basa')->get();
        $bilang_videos = DB::table('videos')->where('video_type', '=', 'bilang')->get();

        if($type == 'basa') {
            return view('pages_teacher.videos_list_all')
                ->with('videos', $basa_videos)
                ->with('type', $type);
        }
        else if($type =='bilang') {
            return view('pages_teacher.videos_list_all')
                ->with('videos', $bilang_videos)
                ->with('type', $type);
        }
    }

    public function play_video($id) {
        $video = DB::table('videos')->where('video_id', '=', $id)->first();
        //dd($video);
        $unit_suggestions = DB::table('videos')->where('unit', '=', $video->unit)->take(4)->get();
        $more_suggestions = DB::table('videos')->where('video_type', '=', $video->video_type)->take(4)->get();

        // dd([$unit_suggestions, $more_suggestions]);
        return view('pages_teacher.play_video')->with('video', $video)
                                            ->with('unit_suggestions', $unit_suggestions)
                                            ->with('more_suggestions', $more_suggestions);
    }

    public function videos_set() {
        return view('pages_teacher.videos_set_describe');
    }

    public function send_feedback() {
        return view('pages_teacher.send_feedback');
    }
    
    public function login_karu() {
        return view('login');
    }

    public function register_karu() {
        return view('signup');
    }
    
    public function error() {
        return view('layouts.error');
    }

    public function error_401() {
        return view('errors.401');
    }
    
    public function error_403() {
        return view('errors.403');
    }
      
    public function error_404() {
        return view('errors.404');
    }

    public function error_500() {
        return view('errors.500');
    }

    public function school_partners() {
        $schools = DB::table('school')->paginate(10);
        return view('pages_admin.partnered_schools')->with('schools', $schools);
    }

    public function view_feedback() {
        $feedback = DB::table('feedback')
                            ->join('users', 'users.id', '=', 'feedback.user_id')
                            ->join('school', 'school.school_id', '=', 'users.school_id')
                            ->paginate(10);
        //dd($feedback);
        return view('pages_admin.view_feedback')->with('feedback', $feedback);
    }

    public function upload_videos() {
        return view('pages_admin.upload_videos');
    }
}
