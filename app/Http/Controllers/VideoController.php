<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
    public function save_video(Request $request) {
        $video = new Video();

        $video->title = $request['title'];
        $video->video_type = $request['video_type'];
        $video->target_grade = $request['grade'];
        $video->desc = $request['desc'];
        $video->unit = $request['unit'];

        if($request->hasFile('thumbnail_path')) {
            $image = $request->thumbnail_path;
            $image_new_name = time().$image->getClientOriginalName();
            $image->move('uploads/basabilang_vids/thumbnails/', $image_new_name);
            $video->thumbnail_path = 'uploads/basabilang_vids/thumbnails/' . $image_new_name;
        }

        if($request->hasFile('video_path')) {
            $vid = $request->video_path;
            $vid_new_name = time().$vid->getClientOriginalName();
            $vid->move('uploads/basabilang_vids/' . $video->video_type . '/', $vid_new_name);
            $video->video_path = 'uploads/basabilang_vids/' . $video->video_type . '/' . $vid_new_name;
        }

        $video->save();
        //dd([$request, $video]);
        return redirect()->action('HomeController@upload_videos');
    }

    public function download_video(Request $request) {
        
    }
}
