<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Announcement;

class AnnouncementController extends Controller
{
    public function store(Request $request) {
        $announcement = new Announcement();

        $announcement->title = $request['title'];
        $announcement->post_body = $request['post_body'];
        $announcement->user_id = Auth::user()->id;
        
        if($request->hasFile('image_path')){
            $image = $request->image_path;
            $image_new_name = time().$image->getClientOriginalName();
            $image->move('uploads/announcement_img/', $image_new_name);
            $announcement->image_path = 'uploads/announcement_img/'.$image_new_name;
        }
        //dd($announcement);
        $announcement->save();
        return redirect()->action('HomeController@home');
    }
}
