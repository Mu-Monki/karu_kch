<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $primaryKey='post_id';
    protected $table='announcement';
    protected $fillable = [
        'title', 'post_body', 'image_path', 'user_id',
    ];
}
