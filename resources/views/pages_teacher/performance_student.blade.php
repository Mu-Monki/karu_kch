@extends('layouts.default')

@section('page_title')
    Karu | Performance     
@endsection

@section('content')

    @section('content_title')
        
        Student Performance

    @endsection

    <div class = "upload-area">
          
        <div class = "upload-performance-button">

            <button type="button" class="btn btn-primary btn-lg">
                Upload Grades
            </button>

        </div>
    
    </div>


@endsection

<script>


</script>