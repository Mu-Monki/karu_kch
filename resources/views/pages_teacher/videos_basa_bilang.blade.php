@extends('layouts.default')

@section('page_title')
    Karu | Videos
@endsection

@section('content')

    @section('content_title')

        Basa Bilang Videos

    @endsection

    {{-- <div class ="videos">
        <section>
                <button type="button" class="btn btn-primary btn-lg">Large button</button> 

        </section>
        <section>videos</section>
        <section>videos of the week</section>
    </div> --}}
    


    {{-- <div class = "row row-eq-height justify-content-center">
      <div class = "col-md-6">
      <div class="card">
        <img src="{{asset('images/500.png')}}" class="card-img-top img-responsive basa-icon-img" alt="Basa icon">
        <div class="card-body">
          <p class="card-text">New vids available, Weeks 1 - 6</p>
        </div>
      </div>
      </div>
      
      <div class = "col-md-6">
      <div class="card">
        <img src="{{asset('images/403.png')}}" class="card-img-top img-responsive basa-icon-img" alt="Basa icon">
        <div class="card-body">
          <p class="card-text">New vids available, Weeks 4 - 6</p>
        </div>
      </div>
      </div>

    </div> --}} 
    <section>
      <div class="container">
        <h1 class ="bb-heading">Select Category</h1>
          <div class="card-wrapper" id = "bb-card-wrap">
           
          <!-- Basa Card -->
            <div class = "card" id ="bb-card">
              <img src="{{asset('images/wikaharian.jpg')}}" alt="" class="basa-bilang-icon">
              <h1>Basa Videos</h1>
              {{-- <p class="bb-content-description">
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                It has survived not only five centuries
              </p> --}}

            <a href="{{url('/videos_all', 'basa')}}" class="btn" id = "bb-btn">Browse</a>

            </div>

          <!-- Bilang Card -->
            <div class = "card" id ="bb-card">
              <img src="{{asset('images/mathdali.jpg')}}" alt="" class="basa-bilang-icon">
              <h1>Bilang Videos</h1>
              {{-- <p class="bb-content-description">
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                It has survived not only five centuries
              </p> --}}

              <a href="{{url('/videos_all', 'bilang')}}" class="btn" id = "bb-btn">Browse</a>

            </div>
            

          </div>
      </div>
    </section>











    
    
    
<!-- End of Body -->
@endsection