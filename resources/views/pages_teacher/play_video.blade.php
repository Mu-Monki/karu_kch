@extends('layouts.default')

@section('page_title')
    Karu | {{ $video->title }} 
@endsection

@section('content')

    @section('content_title')
        
        Video

    @endsection
    <script src="{{ asset('js/video.js') }}"></script>
    <script src="{{ asset('js/videoie.js') }}"></script>
    <style src="{{ asset('css/videojs.css') }}"></style>
    <style>
        .card-margin {
            margin: 5px;
        }
        .card-grp {
            margin-bottom: 20px;
        }
    </style>
    <div class = "row ">
        
        <div class="col-md-8">
            
            <div class = "row">
                <div class = "col-md-12 video-player-padding">
                        <video class="video-player" controls>
                                <source src="{{ url($video->video_path) }}">
                                Your browser does not support HTML5 video.
                        </video>
                </div>
            </div>

            <div class="row">
                
                <div class = "col-md-12 video-player-padding" id = "current-video-title">
                    <h4>{{ $video->title }}</h4>
                        <div class = "row">
                            <div class="col-md-8">
                                <p class="author text-muted date"><i>{{ date('M d, Y', strtotime($video->created_at)) }}</i> &#8226; Grade {{ $video->target_grade }}</p>
                            </div>
                            <div class="col-md-4 text-right video-unit">
                                <p>Unit {{ $video->unit }}<p>
                            </div>
                        </div>
                </div>

            </div>

            <hr>

            <div class="row">
                <div class = "col-md-12 video-player-padding" id = "current-video-title">
                    <p>{{ $video->desc }} 
                    </p>

                </div>
            </div>
            
        </div>

        <div class = "col-md-4 current-video-set video-player-padding">
            <div class="card-grp">
                <h6><b>Unit {{ $video->unit }} Videos</b></h6>
                @foreach ($unit_suggestions as $suggestion)

                    <div class="card card-margin suggestion-item-card" style="max-width: 100%;">
                            <div class="row no-gutters">
                                <div class="col-md-4 vid-thumbnail-container">
                                    <img src="{{ url($suggestion->thumbnail_path) }}" class="suggestion-thumbnail" alt="...">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body suggestion-item-body">
                                        <h5 class="card-title suggestion-item-title"><u><a href="{{ url('/play_video', $suggestion->video_id) }}">{{ $suggestion->title }}</a></u></h5>
                                        <p class="text-muted"> Unit {{ $suggestion->unit }}</p>
                                        {{-- <p class="text-muted"> Recommended For: Grade {{ $vid->target_grade }}</p> --}}
                                        {{-- <p class="author text-muted"><i>{{ date('M d, Y', strtotime($vid->created_at)) }}</i></p> --}}
                                </div>
                            </div> 
                        </div>
                    </div>

                @endforeach
            </div>

                
                <div class="card-grp">
                    <h6><b>Other Videos from {{ ucwords($video->video_type) }}</b></h6>
                    @foreach ($more_suggestions as $suggestion)
                        <div class="card card-margin suggestion-item-card" style="max-width: 100%;">
                                <div class="row no-gutters">
                                    <div class="col-md-4 vid-thumbnail-container">
                                        <img src="{{ url($suggestion->thumbnail_path) }}" class="suggestion-thumbnail" alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body suggestion-item-body">
                                            <h5 class="card-title suggestion-item-title"><u><a href="{{ url('/play_video', $suggestion->video_id) }}">{{ $suggestion->title }}</a></u></h5>
                                            <p class="text-muted"> Unit 1</p>
                                            {{-- <p class="text-muted"> Recommended For: Grade {{ $vid->target_grade }}</p> --}}
                                            {{-- <p class="author text-muted"><i>{{ date('M d, Y', strtotime($vid->created_at)) }}</i></p> --}}
                                    </div>
                                </div> 
                            </div>
                        </div>
                    @endforeach
                </div>
                



        <!-- End of suggested videos column -->
        </div> 



    </div>   
    
    




@endsection

<script>


</script>