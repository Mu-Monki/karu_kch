@extends('layouts.default')

@section('page_title')
    Karu | Feedback    
@endsection

@section('content')

    @section('content_title')

        Send Feedback

    @endsection
    
    <style>
        .containter {
            width: 100%;
        }
        .card {
            width: 100%;
        }

        .emo {
            padding: 15px;
        }

        .input-hidden {
        position: absolute;
        left: -9999px;
        }

        input[type=radio]:checked + label>img.happy {
        border: 1px solid #fff;
        box-shadow: 0 0 5px 5px #CAE7C1;
        }

        input[type=radio]:checked + label>img.sad {
        border: 1px solid #fff;
        box-shadow: 0 0 5px 5px #FF6961;
        }

        input[type=radio]:checked + label>img.smart {
        border: 1px solid #fff;
        box-shadow: 0 0 5px 5px #80CEE1;
        }

        /* Stuff after this is only to make things more pretty */
        input[type=radio] + label>img {
            border-radius: 50px;
            transition: 500ms all;
        }

        input[type=radio]:checked + label>img {
        transform: 
            rotateZ(-380deg) 
            rotateX(10deg);
        }

        .center {
            /* display: block;
            margin-left: auto;
            margin-right: auto;
             */
            text-align: center;
            margin: 0 auto;

        }
        
    </style>

    <div class="container">
    <div class="row justify-content-center">
        <div style="width: 100%">
            <div class="card">
                <div class="card-header">{{ __('Send Feedback to K-Channel') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ action('FeedbackController@store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="post_body" class="col-md-4 col-form-label text-md-right">{{ __('Post Body') }}</label>

                            <div class="col-md-6">
                                <textarea id="post_body" type="text" class="md-textarea form-control @error('post_body') is-invalid @enderror" name="post_body" rows="15" value="{{ old('post_body') }}" required autocomplete="post_body" autofocus></textarea>
                                @error('post_body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="post_body" class="col-md-4 col-form-label text-md-right">{{ __('Feedback Type') }}</label>
    
                                <div class="col-md-6">
                                        <div class="middle">
                                            <div class="card-group">
                                                <div class="card flex-row flex-wrap emo">
                                                    <input 
                                                        type="radio" name="emotion" 
                                                        id="happy" class="input-hidden" value=1 />
                                                    <label for="happy">
                                                        <img 
                                                            src="{{ asset('/images/happy.svg') }}" class="happy"
                                                            alt="I'm happy" width='60px' height='60px' value=1 />
                                                    </label>
                                                    <div class="card-body">
                                                        <p class="card-text"><b>Positive Feedback</b></p>
                                                    </div>
                                                </div>
                                                
                                                <div class="card flex-row flex-wrap emo">
                                                    <input 
                                                        type="radio" name="emotion"
                                                        id="smart" class="input-hidden" value=2 />
                                                    <label for="smart">
                                                        <img 
                                                            src="{{ asset('/images/smart.svg') }}" class="smart"
                                                            alt="I'm smart" width='60px' height='60px' value=2 align='center'/>
                                                    </label>
                                                    <div class="card-body">
                                                        <p class="card-text"><b>Suggestions</b></p>
                                                    </div>
                                                </div>
    
                                                <div class="card flex-row flex-wrap emo">
                                                    <input 
                                                        type="radio" name="emotion"
                                                        id="sad" class="input-hidden" value=3 />
                                                    <label for="sad">
                                                        <img 
                                                            src="{{ asset('/images/sad.svg') }}" class="sad"
                                                            alt="I'm sad" width='60px' height='60px' value=3 />
                                                    </label>
                                                    <div class="card-body">
                                                        <p class="card-text"><b>Negative Feedback</b></p>
                                                    </div>
                                                    </div>
                                            </div>


                                            {{-- <label>
                                            <input type="radio" name="radio" value=1 checked/>
                                            <div class="front-end box">
                                                <span>Positive Feedback</span>
                                            </div>
                                            </label>
                                            <br>
                                            <label>
                                            <input type="radio" name="radio" value=2 />
                                            <div class="back-end box">
                                                <span>Suggestions</span>
                                            </div>
                                            </label>
                                            <br>
                                            <label>
                                                    <input type="radio" name="radio" value=3 />
                                                    <div class="back-end box">
                                                        <span>Negative Feedback</span>
                                                    </div>
                                            </label> --}}
                                        </div>

                                    @error('post_body')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        {{-- <div class="form-group row">
                                <label for="post_img" class="col-md-4 col-form-label text-md-right">{{ __('Upload Image') }}</label>
    
                                <div class="col-md-6">
                                    
                                    <input class="btn" type="file" name="image_path" id="image_path">
    
                                    @error('title')
                                        <span class="invalid-feedback" role="post_img">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div> --}}
                        <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Feedback') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>


@endsection