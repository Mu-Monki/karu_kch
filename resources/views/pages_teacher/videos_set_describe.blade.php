@extends('layouts.default')

@section('content')

    @section('content_title')
        <!-- Title of video group  -->
        Wikaharian Unit 1 
    @endsection
        
    <div class="card mb-3" style="max-width: 100%;">
        <div class="row no-gutters">
            <div class = "col-md-4">
            <img src="{{asset('images/wikaharian.jpg')}}" width="200" height="200" class="card-img" alt="...">
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title"><u>Wikaharian - Pantig</u></h5>
                <p class="author text-muted"><i>Week 1</i></p>
                <p class="card-text">Ang unit na ito ay parte ng limang serye na palabas na tumutgon....</p>
              </div>
            </div>
        </div>
      </div>

      <div class = "row">
        
          <div class="col-md-12">
            
            <h2 class="contentTitle">More videos from week 1.. </h2>
          
          </div>
      
      </div>


      <div class ="row">
      
      <div class="col-md-3">
      <div class="card" style="width: 200px;height: 250px;">
        <img src="{{asset('images/visayas.jpg')}}"width="100" height="100" class="card-img-top" alt="...">
        <div class="card-body">
          <h6 class="card-title">Wikaharian - Pandiwa</h6>
          <p class="card-text">Week 1</p>
        </div>
      </div>
      </div>

      <div class="col-md-3">
      <div class="card" style="width: 200px;height: 250px;">
        <img src="{{asset('images/visayas.jpg')}}"width="100" height="100" class="card-img-top" alt="...">
        <div class="card-body">
            <h6 class="card-title">Wikaharian - Pandiwa</h6>
          <p class="card-text">Week 1</p>
        </div>
      </div>
      </div>

      <div class="col-md-3">
      <div class="card" style="width: 200px;height: 250px;">
        <img src="{{asset('images/visayas.jpg')}}"width="100" height="100" class="card-img-top" alt="...">
        <div class="card-body">
            <h6 class="card-title">Wikaharian - Pandiwa</h6>
          <p class="card-text">Week 1</p>
        </div>
      </div>
      </div>

      <div class="col-md-3">
      <div class="card" style="width: 200px;height: 250px;">
        <img src="{{asset('images/visayas.jpg')}}"width="100" height="100" class="card-img-top" alt="...">
        <div class="card-body">
          <h6 class="card-title">Wikaharian - Pandiwa</h6>
          <p class="card-text">Week 1</p>
        </div>
      </div>
      </div>
      </div>

    
    




@endsection