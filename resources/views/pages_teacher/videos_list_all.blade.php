@extends('layouts.default')

@section('page_title')
    Karu | {{ ucwords($type) }} Videos
@endsection

@section('content')

    @section('content_title')
        All videos
    @endsection

   <section>
        <div class="container">
            <h1 class ="video-group-heading">{{ $type }} Videos</h1> 
                <div class = "card-list">

                    @foreach ($videos as $vid)
                        <div class="card mb-3 video-item-card" style="max-width: 100%;">
                            <div class="row no-gutters">
                                <div class="col-md-4 vid-thumbnail-container">
                                    <img src="{{ url($vid->thumbnail_path) }}" class="video-thumbnail" alt="...">
                                </div>
                                <div class="col-md-6">
                                    <div class="card-body">
                                    <h5 class="card-title"><u><a href="{{url('/play_video', $vid->video_id)}}">{{ $vid->title }}</a></u></h5>
                                        <p class="text-muted"> Unit {{ $vid->unit . ' - ' . date('M d, Y', strtotime($vid->created_at))}} <br> Recommended for Grade {{ $vid->target_grade }}</p>
                                        {{-- <p class="text-muted"> Recommended For: Grade {{ $vid->target_grade }}</p> --}}
                                        {{-- <p class="author text-muted"><i>{{ date('M d, Y', strtotime($vid->created_at)) }}</i></p> --}}
                                    </div>
                                </div>
                                <div class = "col d-flex align-items-center justify-content-center">
                                    <a href="{{ url($vid->video_path) }}" id = "download-videos" download>
                                        <i class="fa fa-download fa-3x" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    
                
                </div>
        </div>
   </section>


    





@endsection