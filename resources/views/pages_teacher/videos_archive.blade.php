@extends('layouts.default')

@section('content')

    @section('content_title')
        Archives
    @endsection

    <!-- Archives Search -->
    <div class = "row">
        <div class = "col-md-6 text-right">
               
        </div>
        <div class = "col-md-6 text-right">
                <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </div>
    </div>

    <!-- Video set title -->
    <div class = "row top-buffer">
            <div class = "col-md-12">
               <h5 id="video-set-title">Basa Videos</h5>
            </div>
    </div>

    <div class = "row top-buffer">
        <div class = "col-md-12">
                <div class="card mb-3" style="max-width: 100%;">
                    <div class="row no-gutters">
                        
                        <!-- Image for card -->
                        <div class="col-md-2">
                            <img src="{{asset('images/wikaharian.jpg')}}" class="card-img" alt="...">
                        </div>

                        <!-- Video title for card -->
                        <div class="col-md-8 d-flex align-items-center justify-content-center">
                            <div class="card-body">
                                <a href="{{ url('/videos_set') }}" id = "download-videos">
                                    <h5 class="card-title">Wikaharian Unit 1</h5>
                                </a>
                            </div>
                        </div>
                    
                        <!-- Download icon for card -->
                        <div class="col d-flex align-items-center justify-content-center">
                                <a href="{{ url('/images/wikaharian.jpg') }}" id = "download-videos" download>
                                    <i class="fa fa-download fa-3x" aria-hidden="true"></i>
                                </a>
                        </div>
                    </div>
                </div>
        </div>
    </div>

    





@endsection