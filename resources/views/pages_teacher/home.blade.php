@extends('layouts.default')

@section('page_title')
    Announcements
@endsection

@section('content')

    @section('content_title')

        Announcements

    @endsection 

    <style>
    
        /* .card {
            display: flex;
        }
        */
        .author {
            font-size: 13;
        }

        .no-content {
          padding-top: 80px;
        }

    </style>
      @if($announcements->isNotEmpty())
      @foreach ($announcements as $announcement)
        <div class="card mb-3" style="max-width: 100%;">
          <div class="row no-gutters">
              <div class="col-md-4">
                <img src="{{ $announcement->image_path }}" class="card-img-home" alt="...">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title"><u>{{ $announcement->title }}</u></h5>
                  <p class="author text-muted"><i>By: {{ $announcement->first_name . ' ' . $announcement->last_name }} | {{ date('M d, Y', strtotime($announcement->created_at)) }}</i></p>
                  <p class="card-text">{{ $announcement->post_body }}</p>
                </div>
              </div>
          </div>
        </div>
      @endforeach
      {{ $announcements->links() }}
      @else
        <div class="no-content">
          <h5 class="text-center">NO ANNOUNCEMENTS POSTED YET...</h5>
        </div>
      @endif
@endsection