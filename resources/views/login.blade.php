<html>

    <head>
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link type="text/css" rel="stylesheet" href="{{asset('css/subStyle.css')}}">    
    </head> 

    <body>
        <div id = "login_background">
            <!-- Orange Background and Login side -->
            <div class = "split left">
                <div class = "centered">
                    <h3>LOGIN</h3>
                </div> 
            </div>

            <div class = "split right">
                <div class = "centered">
                    <h1 id ="main_name">Karu</h1>
                    <form>
                            <div class="form-group row top-buffer center-text">
                              <label for="exampleInputEmail1">Email</label>
                              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                              {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                            </div>
                            <div class="form-group row">
                              <label for="exampleInputPassword1">Password</label>
                              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            

                        
                            <button type="submit" class="btn btn-primary top-buffer center-text btn-lg btn-color">
                                                    Login</button>
                    </form>
                    
                </div>
            </div>








        </div>
    </body>

</html>