@extends('layouts.default')

@section('page_title')
    View Feedback
@endsection
 

@section('content')

    @section('content_title')

        Feedback from Schools

    @endsection 

    <style>
    
        /* .card {
            display: flex;
        }
        */
        .author {
            font-size: 13;
        }

        .no-content {
          padding-top: 80px;
        }

    </style>
    
      @if($feedback->isNotEmpty())
      @foreach ($feedback as $item)
        <div class="card mb-3" style="max-width: 100%;">
          <div class="row no-gutters">
              <div class = "col-md-4">
                <img 
                @if($item->feedback_type == 1)
                    src="{{ asset('images/positive_feedback.png') }}"
                @elseif($item->feedback_type == 2)
                    src="{{ asset('images/suggestions.png') }}"
                @elseif($item->feedback_type ==3)
                    src="{{ asset('images/negative_feedback.png') }}"
                @endif
                    id = "img" class="card-img-feedback img-responsive" alt="...">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title"><u>{{ $item->title }}</u></h5>
                    <p class="text-muted"><i>{{ $item->first_name . ' ' . $item->last_name }} <br> {{ $item->school_name }}</i></p>
                    <p class="card-text">{{ $item->body }}</p>
                </div>
              </div>
          </div>
        </div>
      @endforeach
      @else
        <div class="no-content">
          <h5 class="text-center">NO FEEDBACK GIVEN YET...</h5>
        </div>
      @endif

      {{ $feedback->links() }}

@endsection