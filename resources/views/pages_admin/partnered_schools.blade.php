@extends('layouts.default')

@section('page_title')
    Partnered Schools
@endsection
 

@section('content')

    @section('content_title')

        Basa Bilang Partners

    @endsection 

    <style>
    
        /* .card {
            display: flex;
        }
        */
        .author {
            font-size: 13;
        }

        .no-content {
          padding-top: 80px;
        }

    </style>
      @if($schools->isNotEmpty())
      @foreach ($schools as $school)
        <div class="card mb-3" style="max-width: 100%;">
          <div class="row no-gutters">
              <div>
                <img src="{{ $school->image_path }}" id = "school-img" class="card-img" alt="...">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title"><u>{{ $school->school_name }}</u></h5>
                  <p class="card-text text-muted">{{ $school->location }}</p>
                </div>
              </div>
          </div>
        </div>
      @endforeach
      @else
        <div class="no-content">
          <h5 class="text-center">THERE ARE NO PARTNERED SCHOOLS FOUND IN THE SYSTEM</h5>
        </div>
      @endif

      {{ $schools->links() }}

@endsection