@extends('layouts.default')

@section('page_title')
    Post Announcement
@endsection

@section('content')

    @section('content_title')
        Post Announcement
    @endsection 

<style>
    .containter {
        width: 100%;
    }
    .card {
        width: 100%;
    }
</style>

{{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({selector:'textarea'});
</script> --}}

<div class="container">

        <div class="row justify-content-center">
            <div style="width: 100%">
                <div class="card">
                    <div class="card-header">{{ __('Post Announcement') }}</div>
    
                    <div class="card-body">
                        <form method="POST" action="{{ action('AnnouncementController@store') }}" enctype="multipart/form-data">
                            @csrf
    
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>
    
                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
    
                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="post_body" class="col-md-4 col-form-label text-md-right">{{ __('Post Body') }}</label>
    
                                <div class="col-md-6">
                                    <textarea id="post_body" type="text" class="md-textarea form-control @error('post_body') is-invalid @enderror" name="post_body" rows="15" value="{{ old('post_body') }}" required autocomplete="post_body" autofocus></textarea>
                                    @error('post_body')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label for="post_img" class="col-md-4 col-form-label text-md-right">{{ __('Upload Image') }}</label>
        
                                    <div class="col-md-6">
                                        
                                        <input class="btn" type="file" accept="image/*" name="image_path" id="image_path">
        
                                        @error('title')
                                            <span class="invalid-feedback" role="post_img">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            <br>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Post Announcement') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection