@extends('layouts.default')

@section('page_title')
    Upload Videos
@endsection

@section('content')
    @section('content_title')
        Upload Videos
    @endsection

    <div class="container">

        <style>
            /* .upload {
                background-color: transparent;
            } */
        </style>
               
        <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
        <script src="http://malsup.github.com/jquery.form.js"></script> 

        <div class="row justify-content-center">
            <div style="width: 100%">
                <div class="card">
                    <div class="card-header">{{ __('Upload Video') }}</div>
    
                    <div class="card-body">
                        <form method="POST" action="{{ action('VideoController@save_video') }}" enctype="multipart/form-data">
                            @csrf
    
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Video Title') }}</label>
    
                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
    
                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Video Type') }}</label>
                                
                                <div class="col-md-6">
                                    {{-- <input id="type" type="text" class="form-control @error('video_type') is-invalid @enderror" name="type" value="{{ old('type') }}" required autocomplete="type" autofocus> --}}
                                    <select name="video_type" id="video_type" class="form-control">
                                        <option selected disabled>Select the Video Type</option>
                                        <option value="basa">Basa Video (Reading & Comprehension)</option>
                                        <option value="bilang">Bilang Video (Math & Arithmetics)</option>
                                    </select> 
                                    @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label for="unit" class="col-md-4 col-form-label text-md-right">{{ __('Unit') }}</label>
                                    
                                    <div class="col-md-6">
                                        {{-- <input id="type" type="text" class="form-control @error('video_type') is-invalid @enderror" name="type" value="{{ old('type') }}" required autocomplete="type" autofocus> --}}
                                        <input type="number" min="0" step="1" name="unit" id="unit" class="form-control">
                                        
                                        @error('unit')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                            </div>

                            <div class="form-group row">
                                <label for="grade" class="col-md-4 col-form-label text-md-right">{{ __('Target Grade Level') }}</label>
                                
                                <div class="col-md-6">
                                    <select name="grade" id="grade" class="form-control">
                                        <option selected disabled>Select the Target Grade</option>
                                        <option value=1>Grade 1</option>
                                        <option value=2>Grade 2</option>
                                        <option value=3>Grade 3</option>
                                    </select> 
                                    @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label for="desc" class="col-md-4 col-form-label text-md-right">{{ __('Video Description') }}</label>
                                    
                                    <div class="col-md-6">
                                        {{-- <input id="type" type="text" class="form-control @error('video_type') is-invalid @enderror" name="type" value="{{ old('type') }}" required autocomplete="type" autofocus> --}}
                                        <textarea rows="7" cols="50" name="desc" id="desc" class="form-control"></textarea>
                                        
                                        @error('desc')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                            </div>

                            <div class="form-group row">
                                <label for="post_vid" class="col-md-4 col-form-label text-md-right">{{ __('Upload Video') }}</label>
    
                                <div class="col-md-6">
                                    
                                    <input class="upload-video-btn" id="video" type="file" accept="video/*" name="video_path" id="video_path">
    
                                    @error('video_path')
                                        <span class="invalid-feedback" role="video_path">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="thumbnail" class="col-md-4 col-form-label text-md-right">{{ __('Upload Thumbnail') }}</label>
    
                                <div class="col-md-6">
                                    
                                    <input class="upload-thumbnail-btn" type="file" accept="image/*" name="thumbnail_path">
    
                                    @error('thumbnail_path')
                                        <span class="invalid-feedback" role="thumbnail_path">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div id="target-layer" style="display:none;"></div>

                            <br>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" id="upload" onclick = "upload_btn()">
                                        {{ __('Upload Video') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

@endsection

<script>

function upload_btn(){
    
    Swal.fire({
        title: 'Video uploaded',
        type: 'success',
        confirmButtonText: 'Ok'
    })

}

$(document).ready(function(){
    $('form').ajaxForm({
        beforeSend:function() {
            console.log('before function');
        },
        uploadProgress:function(event, position, total, percentComplete) {
            $('.progress-bar').text(percentComplete + '%');
            $('.progress-bar').css('width', percentComplete + '%');
        },
        success:function(data) {
            if(data.errors) {
                $('.progress-bar').text('0%');
                $('.progress-bar').css('width', '0%');
                Swal.fire({
                    title: data.erorrs,
                    type: 'error',
                    confirmButtonText: 'Ok'
                })
            }
            if(data.success) {
                $('.progress-bar').text('Uploaded');
                $('.progress-bar').css('width', '100%');
                Swal.fire({
                    title: 'Video Uploaded!',
                    type: 'success',
                    confirmButtonText: 'Ok'
                })
            }
        }
    });
});



</script>