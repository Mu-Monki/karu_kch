<html>

<head>
    <title>@yield('page_title')</title>

    <!--CSS links-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
    <!-- Karu CSS -->
    <link rel="stylesheet" href="{{asset('css/subStyle.css')}}">
    
    @yield('page_head')   

</head>

<style>
    /* .btn {
        background-color: #b4b4b4;
    } */
    .center {
        padding: 70px 0;
        text-align: center;
    }
</style>

{{-- <script>

    $('.button').click(function() {
        $.ajax({
            type: "POST",
            url: {{ url() }},
            data: { name: "John" }
        }).done(function( msg ) {
            alert( "Data Saved: " + msg );
        });
    });

</script> --}}

<body>
    <div class="sidenav">
        <div class="center">
            <img src="{{asset('images/profile.png')}}" alt="Avatar" class="avatar" width=150px height =150px>
            <br>
            <h2>{{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</h2>
            <h4>{{ DB::table('users')->join('school', 'users.school_id', '=', 'school.school_id')->find(Auth::user()->id)->school_name }}</h4>
            <br>
            <a href="{{ url('/home') }}" id ="sidenav-home" class='active-page'>Home</a>
            <a href="{{ url('/make_post') }}">Make Announcement</a>
            <a href="{{ url('/videos') }}" class='active-page'>Basa Bilang Videos</a>
            <a href="{{ url('/404') }}" class='active-page'>Student's Performance</a>
            <a href="{{ url('/send_feedback') }}" class='active-page'>Feedback</a>
            <a href="{{ url('/schools') }}"class='active-page'>Basa Bilang Partners</a>
            <a href="{{ url('/view_feedback') }}"class='active-page'>School Feedback</a>
            <a href="{{ url('/upload_videos') }}"class='active-page'>Upload Videos</a>
            <a href="{{ url('/404') }}"class='active-page'>Statistics and Analysis</a>
            <br>
                <div class="container">
                    <form method="POST" action="{{ route('logout') }}" enctype="multipart/form-data">
                    @csrf
                        <input class="btn btn-primary btn-block" type="submit" value="Logout" id ="btn-logout">
                    </form>
                 </div>
        </div>
    </div>

    <div class = "main overflow-control">
            <h2 class = "contentTitle">@yield('content_title')</h2>
    
    
            @yield('content')
    
        </div>


    
        
    <!-- Javascript, Bootstrap CDN -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <!-- SweetAlert JS -->
    <script type="text/javascript" src="{{asset('js/sweetalert.js')}}"></script>
    
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>

    <!-- Font Awesome JavaScript -->
    <script src="https://use.fontawesome.com/73ef3c01f2.js"></script>
    
    <script>
        $('a').on('click', function(){
            $('a').removeClass('selected');
            $(this).addClass('selected');
        });

    </script>



</body>


</html>