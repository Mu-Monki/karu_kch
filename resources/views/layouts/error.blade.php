<html>
<head>
    
    <title>@yield('page_title')</title>

     <!--CSS links-->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
     <!-- Material Design Bootstrap -->
     <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">

    <!-- Karu CSS -->
    <link rel="stylesheet" href="{{asset('css/subStyle.css')}}">

</head>

<body id = "error-body">

    <div class = "split left">
        <div class="centered">
            <h3 id = "error-message-h3">@yield('error_type')</h3>
            <h6 id ="error-message-h6">@yield('error_message')</h6>
            <a href="{{ url('/home') }}">
                <button type="button" class="btn btn-primary btn-lg" id ="error-home-btn">Home</button>
              </a>
        </div>
    </div>

    <div class = "split right">
        <div class="centered">
            @yield('error_image')
        </div>
    </div>
    

</body>

</html>