@extends('layouts.error')

@section('page_title')

Karu | 500

@endsection


@section('error_type')

 500 : Internal Server Error.

@endsection


@section('error_message')

Oops. Our server encountered an error and was unable to complete your request.

@endsection

@section('error_image')

    <img src="{{asset('images/500.png')}}" alt="file icon" class="error-icon-403"> 

@endsection