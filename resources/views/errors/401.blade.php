@extends('layouts.error')

@section('page_title')

Karu | 401

@endsection


@section('error_type')

 401 : Unauthorized Access.

@endsection


@section('error_message')

Please secure authentication before accessing this page.

@endsection

@section('error_image')

<img src="{{asset('images/401.png')}}" alt="file icon" class="error-icon-403"> 

@endsection