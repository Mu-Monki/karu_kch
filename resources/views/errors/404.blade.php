@extends('layouts.error')

@section('page_title')

Karu | 404

@endsection


@section('error_type')

 404 : Page Not Found.

@endsection


@section('error_message')

Looks like the page you were accessing is not here anymore.

@endsection

@section('error_image')

<img src="{{asset('images/404.png')}}" alt="file icon" class="error-icon-404"> 

@endsection