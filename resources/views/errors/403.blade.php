@extends('layouts.error')

@section('page_title')

Karu | 403

@endsection


@section('error_type')

 403 : Forbidden.

@endsection


@section('error_message')

You don't have access to this address on this server.

@endsection

@section('error_image')

<img src="{{asset('images/403.png')}}" alt="file icon" class="error-icon-403"> 

@endsection